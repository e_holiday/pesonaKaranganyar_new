package com.gits.developer.pesonakaranganyar.model.home.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kazt on 12/07/17.
 */

public class CategoryLocationData implements Serializable {
    @SerializedName("cat_id")
    @Expose
    private int cat_id;

    @SerializedName("cat_title")
    @Expose
    private String cat_title;

    @SerializedName("cat_icon")
    @Expose
    private String cat_icon;

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_title() {
        return cat_title;
    }

    public void setCat_title(String cat_title) {
        this.cat_title = cat_title;
    }

    public String getCat_icon() {
        return cat_icon;
    }

    public void setCat_icon(String cat_icon) {
        this.cat_icon = cat_icon;
    }
}
