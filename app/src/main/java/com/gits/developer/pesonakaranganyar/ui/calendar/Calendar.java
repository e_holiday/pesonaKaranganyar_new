package com.gits.developer.pesonakaranganyar.ui.calendar;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.databinding.ActivityCalendarBinding;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kazt on 06/06/17.
 */

public class Calendar extends Fragment {
    private ActivityCalendarBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_calendar,container, false);
        binding.tvCalHeader.setText(getDateNow());
        binding.compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                binding.tvCalHeader.setText(getMontNext(firstDayOfNewMonth));
            }
        });
        return binding.getRoot();
    }

    public String getDateNow(){
        java.util.Calendar cal = java.util.Calendar.getInstance();
        Date dateNow = cal.getTime();
        cal.setTime(dateNow);
        String getBulanNow = new SimpleDateFormat("MMMM").format(cal.getTime());
        String getTahunNow = new SimpleDateFormat("yyyy").format(cal.getTime());
        return getBulanNow + " " + getTahunNow;
    }

    public String getMontNext(Date bulan){
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(bulan);
        String getBulan = new SimpleDateFormat("MMMM").format(cal.getTime());
        String getTahun = new SimpleDateFormat("yyyy").format(cal.getTime());
        return getBulan + " " + getTahun;
    }
}
