package com.gits.developer.pesonakaranganyar.network.service;

import com.gits.developer.pesonakaranganyar.model.home.HomeResponData;
import com.gits.developer.pesonakaranganyar.model.home.category.CategorLocationResponServer;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by kazt on 03/06/17.
 */

public interface HomeService {
    @GET("/api/eholiday/getlocation")
    Call<HomeResponData> getLocation();

    @GET("/api/eholiday/getcategory")
    Call<CategorLocationResponServer> getCategory();
}
