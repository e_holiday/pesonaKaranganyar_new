package com.gits.developer.pesonakaranganyar.ui.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.model.home.category.CategoryLocationData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sasha Grey on 9/6/2016.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolderFilter> {

    private String[] kategori = {"Wisata","Rumah Makan","Hotel","SPBU","Toko","Acara"};
    private int[] icons = {R.drawable.wisata, R.drawable.food, R.drawable.hotel, R.drawable.spbu, R.drawable.toko, R.drawable.agenda};
    private List<CategoryLocationData> listCategory = new ArrayList<>();
    private Context c;

    public FilterAdapter(Context context, List<CategoryLocationData> listCategory) {
        this.listCategory = listCategory;
        this.c = context;
    }

    @Override
    public ViewHolderFilter onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderFilter(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_popup_kategori, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolderFilter holder, int position) {
//        holder.cat.setText(kategori[position]);
//        Glide.with(holder.itemView.getContext())
//                .load(icons[position])
//                .into(holder.img);
        holder.cat.setText(listCategory.get(position).getCat_title());
        Glide.with(c).load(listCategory.get(position).getCat_icon()).fitCenter().into(holder.img);
    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        @BindView(R.id.category_icon)ImageView img;
        @BindView(R.id.category_name)TextView cat;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
