package com.gits.developer.pesonakaranganyar.util;

/**
 * Created by kazt on 23/05/17.
 */

public class NameTag {
    public static final String MARKERDATA = "markerData";
    public static final String TAG_APP_SHARED_PREF = "e_holiday";
    public static final String imgUrl = "imgUrl";
    public static final String username = "username";
    public static final String isLogin = "isLogin";
    public static final String detailPlace = "detailPlace";
    public static final String listPlace = "listPlace";
    public static final String detailNews = "detailNews";
    public static final String tabMana = "tabMana";
    public static final String planTab = "planTab";

    //db constant
    public static final String DBFileName = "plan.db";
    public static final String planTable = "planTable";
    public static final String agendaTab = "agendaTab";

}
