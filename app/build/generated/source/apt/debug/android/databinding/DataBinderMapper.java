
package android.databinding;
import com.gits.developer.pesonakaranganyar.BR;
class DataBinderMapper  {
    final static int TARGET_MIN_SDK = 16;
    public DataBinderMapper() {
    }
    public android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View view, int layoutId) {
        switch(layoutId) {
                case com.gits.developer.pesonakaranganyar.R.layout.activity_login:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityLoginBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.activity_help_detail:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityHelpDetailBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.activity_plan:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityPlanBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.activity_detil_plan:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityDetilPlanBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.item_nomor_sos:
                    return com.gits.developer.pesonakaranganyar.databinding.ItemNomorSosBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.item_rv_help:
                    return com.gits.developer.pesonakaranganyar.databinding.ItemRvHelpBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.activity_help:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityHelpBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.activity_calendar:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityCalendarBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.activity_nomor_sos:
                    return com.gits.developer.pesonakaranganyar.databinding.ActivityNomorSosBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.layout_popup_map:
                    return com.gits.developer.pesonakaranganyar.databinding.LayoutPopupMapBinding.bind(view, bindingComponent);
                case com.gits.developer.pesonakaranganyar.R.layout.item_plan_your_trip:
                    return com.gits.developer.pesonakaranganyar.databinding.ItemPlanYourTripBinding.bind(view, bindingComponent);
        }
        return null;
    }
    android.databinding.ViewDataBinding getDataBinder(android.databinding.DataBindingComponent bindingComponent, android.view.View[] views, int layoutId) {
        switch(layoutId) {
        }
        return null;
    }
    int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        }
        final int code = tag.hashCode();
        switch(code) {
            case -237232145: {
                if(tag.equals("layout/activity_login_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_login;
                }
                break;
            }
            case 117392981: {
                if(tag.equals("layout/activity_help_detail_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_help_detail;
                }
                break;
            }
            case 519560933: {
                if(tag.equals("layout/activity_plan_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_plan;
                }
                break;
            }
            case 1384615566: {
                if(tag.equals("layout/activity_detil_plan_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_detil_plan;
                }
                break;
            }
            case -1816252911: {
                if(tag.equals("layout/item_nomor_sos_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.item_nomor_sos;
                }
                break;
            }
            case 648196198: {
                if(tag.equals("layout/item_rv_help_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.item_rv_help;
                }
                break;
            }
            case 284392701: {
                if(tag.equals("layout/activity_help_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_help;
                }
                break;
            }
            case 16810042: {
                if(tag.equals("layout/activity_calendar_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_calendar;
                }
                break;
            }
            case 1342497165: {
                if(tag.equals("layout/activity_nomor_sos_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.activity_nomor_sos;
                }
                break;
            }
            case -1890280918: {
                if(tag.equals("layout/layout_popup_map_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.layout_popup_map;
                }
                break;
            }
            case 666496659: {
                if(tag.equals("layout/item_plan_your_trip_0")) {
                    return com.gits.developer.pesonakaranganyar.R.layout.item_plan_your_trip;
                }
                break;
            }
        }
        return 0;
    }
    String convertBrIdToString(int id) {
        if (id < 0 || id >= InnerBrLookup.sKeys.length) {
            return null;
        }
        return InnerBrLookup.sKeys[id];
    }
    private static class InnerBrLookup {
        static String[] sKeys = new String[]{
            "_all"
            ,"nomorSOSModel"
            ,"password"
            ,"username"};
    }
}