// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.news;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BeritaActivity_ViewBinding implements Unbinder {
  private BeritaActivity target;

  @UiThread
  public BeritaActivity_ViewBinding(BeritaActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BeritaActivity_ViewBinding(BeritaActivity target, View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.rec_view, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BeritaActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
  }
}
