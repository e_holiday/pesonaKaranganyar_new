// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.home;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeFragment_ViewBinding implements Unbinder {
  private HomeFragment target;

  private View view2131755320;

  private View view2131755321;

  private View view2131755319;

  @UiThread
  public HomeFragment_ViewBinding(final HomeFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.layout_kategori, "method 'showCategory'");
    view2131755320 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showCategory();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_berita, "method 'showNews'");
    view2131755321 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showNews();
      }
    });
    view = Utils.findRequiredView(source, R.id.layout_agenda, "method 'showAgenda'");
    view2131755319 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showAgenda();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view2131755320.setOnClickListener(null);
    view2131755320 = null;
    view2131755321.setOnClickListener(null);
    view2131755321 = null;
    view2131755319.setOnClickListener(null);
    view2131755319 = null;
  }
}
