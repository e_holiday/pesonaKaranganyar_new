package com.gits.developer.pesonakaranganyar.databinding;
import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.BR;
import android.view.View;
public class ItemNomorSosBinding extends android.databinding.ViewDataBinding  {

    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.iv_sos_call, 3);
        sViewsWithIds.put(R.id.ll_phone, 4);
        sViewsWithIds.put(R.id.iv_sos_detail, 5);
    }
    // views
    public final android.widget.ImageView ivSosCall;
    public final android.widget.ImageView ivSosDetail;
    public final android.widget.LinearLayout llPhone;
    private final android.support.v7.widget.CardView mboundView0;
    public final android.widget.TextView tvSosName;
    public final android.widget.TextView tvSosNumber;
    // variables
    private com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData mNomorSOSModel;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemNomorSosBinding(android.databinding.DataBindingComponent bindingComponent, View root) {
        super(bindingComponent, root, 0);
        final Object[] bindings = mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds);
        this.ivSosCall = (android.widget.ImageView) bindings[3];
        this.ivSosDetail = (android.widget.ImageView) bindings[5];
        this.llPhone = (android.widget.LinearLayout) bindings[4];
        this.mboundView0 = (android.support.v7.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvSosName = (android.widget.TextView) bindings[1];
        this.tvSosName.setTag(null);
        this.tvSosNumber = (android.widget.TextView) bindings[2];
        this.tvSosNumber.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean setVariable(int variableId, Object variable) {
        switch(variableId) {
            case BR.nomorSOSModel :
                setNomorSOSModel((com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData) variable);
                return true;
        }
        return false;
    }

    public void setNomorSOSModel(com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData NomorSOSModel) {
        this.mNomorSOSModel = NomorSOSModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.nomorSOSModel);
        super.requestRebind();
    }
    public com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData getNomorSOSModel() {
        return mNomorSOSModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.gits.developer.pesonakaranganyar.model.nomorsos.SOSContactData nomorSOSModel = mNomorSOSModel;
        java.lang.String nomorSOSModelNumber = null;
        java.lang.String nomorSOSModelName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (nomorSOSModel != null) {
                    // read nomorSOSModel.number
                    nomorSOSModelNumber = nomorSOSModel.getNumber();
                    // read nomorSOSModel.name
                    nomorSOSModelName = nomorSOSModel.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.tvSosName, nomorSOSModelName);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.tvSosNumber, nomorSOSModelNumber);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    public static ItemNomorSosBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ItemNomorSosBinding inflate(android.view.LayoutInflater inflater, android.view.ViewGroup root, boolean attachToRoot, android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ItemNomorSosBinding>inflate(inflater, com.gits.developer.pesonakaranganyar.R.layout.item_nomor_sos, root, attachToRoot, bindingComponent);
    }
    public static ItemNomorSosBinding inflate(android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ItemNomorSosBinding inflate(android.view.LayoutInflater inflater, android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.gits.developer.pesonakaranganyar.R.layout.item_nomor_sos, null, false), bindingComponent);
    }
    public static ItemNomorSosBinding bind(android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    public static ItemNomorSosBinding bind(android.view.View view, android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/item_nomor_sos_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ItemNomorSosBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): nomorSOSModel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}