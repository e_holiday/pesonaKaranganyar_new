// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.home;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import com.gits.developer.pesonakaranganyar.ui.CirclePagesIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DestinasiDetailActivity_ViewBinding implements Unbinder {
  private DestinasiDetailActivity target;

  @UiThread
  public DestinasiDetailActivity_ViewBinding(DestinasiDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DestinasiDetailActivity_ViewBinding(DestinasiDetailActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.collapsingToolbarLayout = Utils.findRequiredViewAsType(source, R.id.collapsing, "field 'collapsingToolbarLayout'", CollapsingToolbarLayout.class);
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.viewpager, "field 'viewPager'", ViewPager.class);
    target.circlePagesIndicator = Utils.findRequiredViewAsType(source, R.id.circle_indicator, "field 'circlePagesIndicator'", CirclePagesIndicator.class);
    target.appBarLayout = Utils.findRequiredViewAsType(source, R.id.appbar, "field 'appBarLayout'", AppBarLayout.class);
    target.recViewNearby = Utils.findRequiredViewAsType(source, R.id.rec_view_nearby, "field 'recViewNearby'", RecyclerView.class);
    target.tvLokasiDekat = Utils.findRequiredViewAsType(source, R.id.tv_lokasi_dekat, "field 'tvLokasiDekat'", TextView.class);
    target.tvDescPlace = Utils.findRequiredViewAsType(source, R.id.tv_desc_place, "field 'tvDescPlace'", TextView.class);
    target.cvPlan = Utils.findRequiredViewAsType(source, R.id.cv_plan, "field 'cvPlan'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DestinasiDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.collapsingToolbarLayout = null;
    target.viewPager = null;
    target.circlePagesIndicator = null;
    target.appBarLayout = null;
    target.recViewNearby = null;
    target.tvLokasiDekat = null;
    target.tvDescPlace = null;
    target.cvPlan = null;
  }
}
