// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.kritiksaran;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class KritikSaranFragment_ViewBinding implements Unbinder {
  private KritikSaranFragment target;

  private View view2131755325;

  @UiThread
  public KritikSaranFragment_ViewBinding(final KritikSaranFragment target, View source) {
    this.target = target;

    View view;
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.email, "field 'txtEmail'", TextInputEditText.class);
    target.txtName = Utils.findRequiredViewAsType(source, R.id.name, "field 'txtName'", TextInputEditText.class);
    target.txtKomentar = Utils.findRequiredViewAsType(source, R.id.komentar, "field 'txtKomentar'", TextInputEditText.class);
    view = Utils.findRequiredView(source, R.id.btn_kirim, "method 'chooseEmail'");
    view2131755325 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.chooseEmail();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    KritikSaranFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtEmail = null;
    target.txtName = null;
    target.txtKomentar = null;

    view2131755325.setOnClickListener(null);
    view2131755325 = null;
  }
}
