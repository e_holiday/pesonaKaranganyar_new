// Generated code from Butter Knife. Do not modify!
package com.gits.developer.pesonakaranganyar.ui.home;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gits.developer.pesonakaranganyar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterAdapter$ViewHolderFilter_ViewBinding implements Unbinder {
  private FilterAdapter.ViewHolderFilter target;

  @UiThread
  public FilterAdapter$ViewHolderFilter_ViewBinding(FilterAdapter.ViewHolderFilter target,
      View source) {
    this.target = target;

    target.img = Utils.findRequiredViewAsType(source, R.id.category_icon, "field 'img'", ImageView.class);
    target.cat = Utils.findRequiredViewAsType(source, R.id.category_name, "field 'cat'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterAdapter.ViewHolderFilter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.img = null;
    target.cat = null;
  }
}
